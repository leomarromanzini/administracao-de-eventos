import DrinkBox from "../../components/DrinkBox";
import DropButton from "../../components/DropButton";

import { Text, Image } from "grommet";
import { useDrinks } from "../../Providers/DrinksList";
import { useWeddingCart } from "../../Providers/WeddingCart";

const CatalogueDrinks = () => {
  const { drinks } = useDrinks();
  const { weddingCart } = useWeddingCart();

  console.log(drinks);
  return (
    <>
      {drinks.map((drink) => (
        <DrinkBox>
          <Image fit="cover" src={drink.image_url} />
          <Text size="small" style={{ textAlign: "justify" }}>
            {drink.description}
          </Text>

          <Text size="small">
            {drink.volume.unit} {drink.volume.value}{" "}
          </Text>
          <Text size="small"> {drink.first_brewed}</Text>
          <Text weight="bold">{drink.name}</Text>

          <DropButton item={drink} />
        </DrinkBox>
      ))}
    </>
  );
};

export default CatalogueDrinks;
