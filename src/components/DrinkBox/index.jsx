import { Box } from "grommet";

const DrinkBox = ({ children }) => {
  return (
    <Box
      width="medium"
      height="600px"
      round="small"
      align="center"
      justify="center"
      background="secondary"
      gap="small"
      margin="small"
      pad="medium"
      elevation="medium"
    >
      {children}
    </Box>
  );
};

export default DrinkBox;
