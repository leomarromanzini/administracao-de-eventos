import styled from "styled-components";
import { Bar } from "grommet-icons";
import { Heading } from "grommet";
export const MyCustomBar = styled(Bar)`
  margin-left: 10px;
`;

export const CustomHeading = styled(Heading)`
  /* font-variant: small-caps; */
  font-family: "Hind Madurai", sans-serif;

  font-weight: 600;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 18px 0;

  @media (max-width: 760px) {
    padding: 30px;
    height: 500px;
  }

  @media (max-width: 400px) {
    font-size: 23px;
    padding: 15px 0;
    .iconHeader {
      margin-right: 3px;
      margin-left: 10px;
    }
  }
  @media (max-width: 350px) {
    .iconHeader {
      display: none;
    }
  }
`;
