import { Box, Button } from "grommet";
import { Calendar } from "grommet-icons";
import { useHistory } from "react-router";
import { MyCustomBar, CustomHeading } from "./styles";
const AppBar = (props) => (
  <Box
    tag="header"
    direction="row"
    align="center"
    justify="between"
    background="brand"
    pad={{ left: "medium", right: "small", vertical: "small" }}
    elevation="medium"
    style={{ zIndex: "1" }}
    {...props}
  />
);

const Header = ({ setShowSidebar, showSidebar, page }) => {
  const history = useHistory();
  return (
    <AppBar>
      <CustomHeading
        onClick={() => history.push("./")}
        style={{ cursor: "pointer" }}
        size="2"
      >
        Bebidas
        <MyCustomBar className="iconHeader" />
      </CustomHeading>
      <Button onClick={() => setShowSidebar(!showSidebar)}>
        <CustomHeading size="2">
          Eventos
          <Calendar
            className="iconHeader"
            style={{ marginLeft: "15px", marginRight: "15px" }}
          />
        </CustomHeading>
      </Button>
    </AppBar>
  );
};

export default Header;
