import { Text, Image, Box, Button, Heading } from "grommet";
import { Trash } from "grommet-icons";

const EventsBox = ({ drink, removeDrink }) => {
  return (
    <Box
      width="large"
      height="small"
      direction="row"
      background="secondary"
      gap="small"
      margin="small"
      pad="xsmall"
      justify="center"
      align="center"
      elevation="medium"
    >
      <Image
        style={{ maxWidth: 80, maxHeight: 160 }}
        fit="contain"
        src={drink.image_url}
      />
      <Heading size="1" weight="bold">
        {drink.name}
      </Heading>
      <Text size="large">
        {drink.volume.unit} {drink.volume.value}{" "}
      </Text>

      <Button
        onClick={() => removeDrink(drink)}
        icon={<Trash style={{ width: 32, height: 32 }} />}
      />
    </Box>
  );
};

export default EventsBox;
