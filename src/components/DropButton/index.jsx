import React from "react";
import PropTypes from "prop-types";
import { Close } from "grommet-icons";
import { customTheme } from "../../styles/global";
import { Grommet, Box, Button, DropButton, Heading } from "grommet";

import { useWeddingCart } from "../../Providers/WeddingCart";
import { useGraduationCart } from "../../Providers/GraduationCart";
import { useConfraternizationCart } from "../../Providers/ConfraternizationCart";

const DropButtons = ({ item }) => {
  const { addToWeeding } = useWeddingCart();
  const { addToGraduation } = useGraduationCart();
  const { addToConfraternization } = useConfraternizationCart();

  const DropContent = ({ onClose }) => (
    <Grommet theme={customTheme}>
      <Box pad="small">
        <Box direction="row" justify="between" align="center">
          <Heading level={3} margin="small">
            Evento
          </Heading>

          <Button icon={<Close />} onClick={onClose} />
        </Box>

        <Button
          onClick={() => addToWeeding(item)}
          secondary
          label="Casamento + 1"
        />
        <Button
          onClick={() => addToGraduation(item)}
          style={{ margin: " 8px 0" }}
          secondary
          label="Formatura + 1"
        />
        <Button
          onClick={() => addToConfraternization(item)}
          secondary
          label="Confraternização + 1"
        />
      </Box>
    </Grommet>
  );

  DropContent.propTypes = {
    onClose: PropTypes.func.isRequired,
  };

  const [open, setOpen] = React.useState();
  const onOpen = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };

  return (
    <DropButton
      primary
      label="Adcionar"
      open={open}
      onOpen={onOpen}
      onClose={onClose}
      dropContent={<DropContent onClose={onClose} />}
      dropProps={{ align: { top: "bottom" } }}
    />
  );
};

export default DropButtons;
