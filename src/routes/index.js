import { Switch, Route } from "react-router-dom";
import Home from "../pages/Home";
import Wedding from "../pages/Wedding";
import Confraternization from "../pages/Confraternization";
import Graduation from "../pages/Graduation";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>

      <Route path="/wedding">
        <Wedding />
      </Route>
      <Route path="/Confraternization">
        <Confraternization />
      </Route>
      <Route path="/Graduation">
        <Graduation />
      </Route>
    </Switch>
  );
};

export default Routes;
