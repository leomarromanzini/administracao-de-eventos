import { grommet } from "grommet";

import { deepMerge } from "grommet/utils";

export const theme = {
  global: {
    colors: {
      brand: "#403F4C",
      secondary: "#F9DC5C",
      // 70D6FF  7D70BA
    },
    font: {
      family: "Roboto",
      size: "18px",
      height: "20px",
    },
  },
  heading: {
    extend: (props) => props,
  },
};

export const customTheme = deepMerge(grommet, {
  button: {
    default: {},
    hover: {
      default: { background: "background-contrast" },
      secondary: {
        border: {
          width: "3px",
        },
      },
    },
    secondary: {
      border: {
        color: "#2196f3",
        width: "2px",
      },
    },
    border: {
      radius: "5px",
    },
    size: {
      medium: {
        pad: {
          vertical: "6px",
          horizontal: "12px",
        },
      },
    },
  },
});
