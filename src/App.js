import { Home } from "grommet-icons";

import Wedding from "./pages/Wedding";
import Drinks from "./components/CatalogueDrinks";
import Routes from "./routes";

function App() {
  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
