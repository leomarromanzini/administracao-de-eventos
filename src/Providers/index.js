import { ConfraternizationCartProvider } from "./ConfraternizationCart";
import { DrinkListProvider } from "./DrinksList";
import { GraduationCartProvider } from "./GraduationCart";
import { WeddingCartProvider } from "./WeddingCart";

const Providers = ({ children }) => {
  return (
    <WeddingCartProvider>
      <ConfraternizationCartProvider>
        <GraduationCartProvider>
          <DrinkListProvider>{children}</DrinkListProvider>
        </GraduationCartProvider>
      </ConfraternizationCartProvider>
    </WeddingCartProvider>
  );
};

export default Providers;
