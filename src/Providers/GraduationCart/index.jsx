import { createContext, useContext, useState } from "react";

const GraduationContex = createContext();

export const GraduationCartProvider = ({ children }) => {
  const [graduationCart, setGraduationCart] = useState([]);

  const addToGraduation = (product) => {
    setGraduationCart([...graduationCart, product]);
  };

  const removeFromGraduation = (item) => {
    setGraduationCart(
      graduationCart.filter((product) => product.id !== item.id)
    );
  };

  return (
    <GraduationContex.Provider
      value={{ graduationCart, addToGraduation, removeFromGraduation }}
    >
      {children}
    </GraduationContex.Provider>
  );
};

export const useGraduationCart = () => useContext(GraduationContex);
