import { createContext, useContext, useEffect, useState } from "react";
import api from "../../services/api";

const DrinkListContext = createContext([]);

export const DrinkListProvider = ({ children }) => {
  const [drinks, setDrinks] = useState([]);
  useEffect(() => {
    if (!drinks.length) {
      api.get().then((response) => setDrinks(response.data));
    }
  }, [drinks]);

  return (
    <DrinkListContext.Provider value={{ drinks }}>
      {children}
    </DrinkListContext.Provider>
  );
};

export const useDrinks = () => useContext(DrinkListContext);
