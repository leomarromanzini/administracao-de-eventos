import { createContext, useContext, useState } from "react";

const CartWeddingContex = createContext();

export const WeddingCartProvider = ({ children }) => {
  const [weddingCart, setWeddingCart] = useState([]);

  const addToWeeding = (product) => {
    setWeddingCart([...weddingCart, product]);
  };

  const removeFromWedding = (item) => {
    setWeddingCart(weddingCart.filter((product) => product.id !== item.id));
  };

  return (
    <CartWeddingContex.Provider
      value={{ weddingCart, addToWeeding, removeFromWedding }}
    >
      {children}
    </CartWeddingContex.Provider>
  );
};

export const useWeddingCart = () => useContext(CartWeddingContex);
