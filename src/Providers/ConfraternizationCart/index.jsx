import { createContext, useContext, useState } from "react";

const ConfraternizationContex = createContext();

export const ConfraternizationCartProvider = ({ children }) => {
  const [confraternizationCart, setConfraternizationCart] = useState([]);

  const addToConfraternization = (product) => {
    setConfraternizationCart([...confraternizationCart, product]);
  };

  const removeFromConfraternization = (item) => {
    setConfraternizationCart(
      confraternizationCart.filter((product) => product.id !== item.id)
    );
  };

  return (
    <ConfraternizationContex.Provider
      value={{
        confraternizationCart,
        addToConfraternization,
        removeFromConfraternization,
      }}
    >
      {children}
    </ConfraternizationContex.Provider>
  );
};

export const useConfraternizationCart = () =>
  useContext(ConfraternizationContex);
