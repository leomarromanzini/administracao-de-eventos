import styled from "styled-components";
import { Box, Heading } from "grommet";

export const CustomHeading = styled(Heading)`
  /* font-variant: small-caps; */
  font-family: "Hind Madurai", sans-serif;

  border: 3px solid #2196f3;
  margin: 30px 0 0 0;
  font-size: 47px;
  padding: 5px;
  font-variant: small-caps;
  border-radius: 5px;
  width: 329px;
  text-align: center;
  cursor: pointer;

  @media (max-width: 940px) {
    font-size: 34px;
    width: 250px;
  }

  @media (max-width: 380px) {
    width: 250px;
    font-size: 35px;
  }
`;

export const Tittle = styled.h2`
  background: #81fced;
  color: #444444;
  display: flex;
  padding-top: 30px;
  padding-bottom: 20px;
  border-bottom: 1px solid #999999;

  margin: 0;
  font-size: 60px;
  font-variant: small-caps;
  font-family: "Hind Madurai", sans-serif;
  width: 70%;
  justify-content: center;
`;

export const CustomBox = styled(Box)`
  @media (max-width: 1300px) {
    width: 50vw;
  }

  @media (max-width: 1100px) {
    width: 60vw;
  }
`;
