import {
  Box,
  Button,
  Collapsible,
  Heading,
  Grommet,
  ResponsiveContext,
  Layer,
} from "grommet";

import { theme } from "../../styles/global";
import { FormClose } from "grommet-icons";
import { useState } from "react";
import Header from "../../components/Header";

import { CustomBox, CustomHeading, Tittle } from "./styles";

import { useHistory } from "react-router";
import CatalogueDrinks from "../../components/CatalogueDrinks";

const Home = () => {
  const history = useHistory();

  const sendTo = (path) => {
    history.push(path);
  };
  const [showSidebar, setShowSidebar] = useState(false);
  return (
    <Grommet theme={theme} full>
      <ResponsiveContext.Consumer>
        {(size) => (
          <Box fill>
            <Header
              showSidebar={showSidebar}
              setShowSidebar={setShowSidebar}
              page="Bebidas"
            />

            <Box
              direction="row-responsive"
              flex
              overflow={{ horizontal: "hidden" }}
              background="#81FCED"
            >
              <div style={{ width: "100%" }}>
                <div
                  style={{
                    background: "#81fced",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Tittle>Bebidas</Tittle>
                </div>
                <Box
                  flex
                  direction="row"
                  pad={{ bottom: "10%", top: "20px" }}
                  justify="center"
                  wrap="true"
                >
                  <CatalogueDrinks />
                </Box>
              </div>

              {!showSidebar || size !== "small" ? (
                <Collapsible direction="horizontal" open={showSidebar}>
                  <CustomBox
                    flex
                    background="light-2"
                    elevation="small"
                    align="center"
                    width="490px"
                  >
                    <CustomHeading onClick={() => sendTo("/Confraternization")}>
                      Confraternização
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/wedding")}>
                      Casamento
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/Graduation")}>
                      Formatura
                    </CustomHeading>
                  </CustomBox>
                </Collapsible>
              ) : (
                <Layer>
                  <Box
                    background="light-2"
                    tag="header"
                    justify="end"
                    align="center"
                    direction="row"
                  >
                    <Heading
                      fontVariant="small-caps"
                      margin="none"
                      style={{
                        margin: "0 auto",
                        transform: "translateX(30%)",
                      }}
                    >
                      Menu
                    </Heading>
                    <Button
                      icon={<FormClose />}
                      onClick={() => setShowSidebar(false)}
                    />
                  </Box>

                  <Box fill background="light-2" align="center">
                    <CustomHeading onClick={() => sendTo("/Confraternization")}>
                      Confraternização
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/wedding")}>
                      Casamento
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/Graduation")}>
                      Formatura
                    </CustomHeading>
                  </Box>
                </Layer>
              )}
            </Box>
          </Box>
        )}
      </ResponsiveContext.Consumer>
    </Grommet>
  );
};

export default Home;
