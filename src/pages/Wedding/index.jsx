import {
  Box,
  Button,
  Collapsible,
  Heading,
  Grommet,
  ResponsiveContext,
  Layer,
} from "grommet";

import { theme } from "../../styles/global";
import { FormClose } from "grommet-icons";
import { useState } from "react";
import Header from "../../components/Header";

import { CustomHeading, Tittle, CustomBox } from "../Home/styles";

import { useHistory } from "react-router";
import { useWeddingCart } from "../../Providers/WeddingCart";
import EventsBox from "../../components/EventsBox";

const Wedding = () => {
  const history = useHistory();

  const { weddingCart, removeFromWedding } = useWeddingCart();

  const sendTo = (path) => {
    history.push(path);
  };
  const [showSidebar, setShowSidebar] = useState(false);

  console.log(weddingCart);

  return (
    <Grommet theme={theme} full>
      <ResponsiveContext.Consumer>
        {(size) => (
          <Box fill>
            <Header showSidebar={showSidebar} setShowSidebar={setShowSidebar} />
            <Box
              direction="row-responsive"
              flex
              overflow={{ horizontal: "hidden" }}
              background="#81FCED"
            >
              <div style={{ width: "100%" }}>
                <div
                  style={{
                    background: "#81fced",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <Tittle>Casamento</Tittle>
                </div>
                <Box
                  flex
                  direction="column"
                  pad={{ bottom: "10%", top: "20px" }}
                  align="center"
                  wrap="true"
                >
                  {weddingCart.map((drink) => (
                    <EventsBox drink={drink} removeDrink={removeFromWedding} />
                  ))}
                </Box>
              </div>

              {!showSidebar || size !== "small" ? (
                <Collapsible direction="horizontal" open={showSidebar}>
                  <CustomBox
                    flex
                    width="480px"
                    background="light-2"
                    elevation="small"
                    align="center"
                  >
                    <CustomHeading onClick={() => sendTo("/Confraternization")}>
                      Confraternização
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/wedding")}>
                      Casamento
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/Graduation")}>
                      Formatura
                    </CustomHeading>
                  </CustomBox>
                </Collapsible>
              ) : (
                <Layer>
                  <Box
                    background="light-2"
                    tag="header"
                    justify="end"
                    align="center"
                    direction="row"
                  >
                    <Heading
                      fontVariant="small-caps"
                      margin="none"
                      style={{
                        margin: "0 auto",
                        transform: "translateX(30%)",
                      }}
                    >
                      Menu
                    </Heading>
                    <Button
                      icon={<FormClose />}
                      onClick={() => setShowSidebar(false)}
                    />
                  </Box>

                  <Box fill background="light-2" align="center">
                    <CustomHeading onClick={() => sendTo("/Confraternization")}>
                      Confraternização
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/wedding")}>
                      Casamento
                    </CustomHeading>
                    <CustomHeading onClick={() => sendTo("/Graduation")}>
                      Formatura
                    </CustomHeading>
                  </Box>
                </Layer>
              )}
            </Box>
          </Box>
        )}
      </ResponsiveContext.Consumer>
    </Grommet>
  );
};

export default Wedding;
